.. csv-table:: **How was the experiment?**
   :header: "Complexity", "Count"
   :widths: 60, 20

   "Too Simple, I am wasting time", "2"
   "Good, But Not Challenging for me", "1"
   "Good and Challenging for me", "2"
   "Was Tough, but I did it", "1"
   "Too Difficult for me", "2"


.. csv-table:: **Text and Image description/explanation and code comments within the experiment:**
   :header: "Feedback experiments input", "Count"
   :widths: 60, 20

   "Very Useful", "2"
   "Somewhat Useful", "1"
   "Not Useful", "2"
   "Didn't use", "1"


.. csv-table:: **Mentor support**
   :header: "Feedback mentor support", "Count"
   :widths: 60, 20

   "Very Useful", "2"
   "Somewhat Useful", "1"
   "Not Useful", "2"
   "Didn't use", "1"


.. csv-table:: **Can you identify the concepts from 
               the lecture which this experiment covered?**
   :header: "Concepts", "Count"
   :widths: 60, 20

   "YES", "2"
   "NO", "1"
