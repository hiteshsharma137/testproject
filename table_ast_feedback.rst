+-------------------------+--------------+----------------------------+-------------------------+-------------+
| Question                | Complexity   | Feedback Experiments Input | Feedback Mentor Support | Concepts    |
+=========================+==============+============================+=========================+=============+
| How was the experiment? | Remark-Value | Remark-Value               |    Remark-Value         | Remark-Value|
+-------------------------+--------------+----------------------------+-------------------------+-------------+
| Text and Image          | Remark-Value |     Remark-Value           |   Remark-Valu           | Remark-Value|
| description/explanation |              |                            |                         |             |
| and code comments within|              |                            |                         |             |
| the experiment:         |              |                            |                         |             |
+-------------------------+--------------+----------------------------+-------------------------+-------------+
| Mentor support          | Remark-Value |       Remark-Value         |    Remark-Value         | Remark-Value|
+-------------------------+--------------+----------------------------+-------------------------+-------------+
| Can you identify the    |              |                            |                         |Remark-Value |
| concepts from           |              |                            |                         |             |
| the lecture which       |              |   Remark-Value             |     Remark-Value        |             |
| this experiment         |              |                            |                         |             |
| covered?                | blocks       |                            |                         |             |
+-------------------------+--------------+----------------------------+-------------------------+-------------+
