.. image:: https://cdn.extras.talentsprint.com/CentralRepo/images/TS_updated_logo.png
  :width: 200

===============================
Linear Regression + SGD
===============================
This is part of the official standard documentation of TalentSprint.
Except when explicitly specified, the contents are copyrighted intellectual
property of TalentSprint Private Limited.

Metadata
--------

:Description: Week 1: Linear Regression + SGD

:Author: Hitesh Sharma

:Version: 1.0.1


Purpose
-------
    This document captures the session-level information for week-1 lecture in Module-2 -  Machine Learning

    
Template
--------

:Metadata: As above

:Session Information: It should be in the following format

+-----------------------+--------------------------------------------------+
|**Course Name**        | CDS                                              |
+-----------------------+--------------------------------------------------+
|**Module Name**        | Machine Learning                                 |
+-----------------------+--------------------------------------------------+
|**Session No**         | Week-1                                           |
+-----------------------+--------------------------------------------------+
|**Session Name**       | Linear Regression + SGD                          |
+-----------------------+--------------------------------------------------+
|**Session Type**       | Learn/Practice                                   |
+-----------------------+--------------------------------------------------+
|**Session Duration**	| 210 Minutes:                                     |
|                       |      - 180 Minutes  -  Instructor  Led Training  |
|                       |      - 30 Minutes - Break                        |   
|                       |                                                  |
+-----------------------+--------------------------------------------------+


:Objectives: Instructional Design Objectives for the session.

:Instructor  Led training - Session Break-up: It should be in the following format

+-----+--------------------------------------------------+---------------+
|Sno  |   Topic                                          |Duration(Mins) |
+-----+--------------------------------------------------+---------------+
|1    | ML Workflow                                      |     65        |
+-----+--------------------------------------------------+---------------+
|2    | Linear Regression                                |     30        |
+-----+--------------------------------------------------+---------------+
|3    | Normal Equation                                  |     30        |
+-----+--------------------------------------------------+---------------+
|4    | SGD Regressor                                    |     55        |
+-----+--------------------------------------------------+---------------+


