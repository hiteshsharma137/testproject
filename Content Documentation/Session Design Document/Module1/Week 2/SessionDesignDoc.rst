.. image:: https://cdn.extras.talentsprint.com/CentralRepo/images/TS_updated_logo.png
  :width: 200

====================================
Classification: Logistic Regression
====================================
This is part of the official standard documentation of TalentSprint.
Except when explicitly specified, the contents are copyrighted intellectual
property of TalentSprint Private Limited.

Metadata
--------

:Description: Week 2: Classification: Logistic Regression

:Author: Hitesh Sharma

:Version: 1.0.1


Purpose
-------
    This document captures the session-level information for week-2 lecture in Module-2 -  Machine Learning

    
Template
--------

:Metadata: As above

.. csv-table:: **Session Information**
   :header: "", "", "", "", "", ""
   :widths: 30, 60

   "**Course Name**", "PG Level Advanced Certification in Computational Data Science"
   "**Module Name**", "Machine Learning"
   "**Session No**", "Week-2"
   "**Session Name**", "Classification: Logistic Regression"
   "**Session Type**", "Learn/Practice"
   "**Session Duration**", "210 Minutes:
                              # - 180 Minutes  -  Instructor  Led Training
                              # - 30 Minutes - Break"


:Objectives: Instructional Design Objectives for the session.

:Instructor  Led training - Session Break-up: It should be in the following format

+-----+--------------------------------------------------+---------------+
|Sno  |   Topic                                          |Duration(Mins) |
+-----+--------------------------------------------------+---------------+
|1    | ML Workflow                                      |     65        |
+-----+--------------------------------------------------+---------------+
|2    | Linear Regression                                |     30        |
+-----+--------------------------------------------------+---------------+
|3    | Normal Equation                                  |     30        |
+-----+--------------------------------------------------+---------------+
|4    | SGD Regressor                                    |     55        |
+-----+--------------------------------------------------+---------------+


