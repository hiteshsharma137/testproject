.. image:: https://cdn.extras.talentsprint.com/CentralRepo/images/TS_updated_logo.png
  :width: 200

===============================
Module Design Document Template
===============================
This is part of the official standard documentation of TalentSprint.
Except when explicitly specified, the contents are copyrighted intellectual
property of TalentSprint Private Limited.

Metadata
--------

:Description: Module Design Document Template

:Author: Asokan Pichai

:Version: 1.0.1

:See also: Core_Docs --> RepoLayout


Purpose
-------
    This document is the template for all Module Design Documents

    
Template
--------

:Metadata: As above

:Module Name: Foundations of Machine Learning and Artificial Intelligence

:Overview: This module focuses on laying the foundation for Machine Learning.

:Objectives:
  - The ML Process - How to solve a problem using data and algorithms?
  - What are the problems solvable by ML/AI? What cannot be solved?
  - Data Types and State of the Art Models
     - Tabular Data - Gradient Boosted Models
     - Image Data - Convolutional Neural Networks
     - Sequential and Time Series Data - Recurrent Neural Networks
     - Text Data - Transformers
     - Cool Applications - Generative Models, GANs
     - Robotics and other niche areas - Reinforcement Learning
  - Decision Tree and Gradient Boosted Models - State of the Art for Tabular Datasets
  - The first neural network - A very shallow sigmoidal NN (or Logistic Regression)
  - The Mathematics of ML and AI - Empirical Risk Minimization, Gradient Descent and Back Propagation
  - A Deep Neural Network: Neurons, Layers, Activation Function, Loss Function, Weights and Biases, Minibatch, Training Algorithms (Momentum, AdaGrad, ADAM), Weight Initialization
  - Keras: Finding data, building a model, training a model, model evaluation
  - Deep Dive into model selection, evaluation and fine-tuning

:Assessment Strategies: 
  - 2 Mini-projects
     - Employee Attrition Prediction using XGBoost | Part - A and B
     - Structured Data Classification using Keras | Part - A and B
  - 3 Assignments
     - Scikit-Learn Pipeline
     - XGBoost
     - MNIST Keras
  - 4 Weekly Quizzes
  - 1 LongQuiz for Module 1&2 combined

:Sessions: 
  - Week 1: 
     - Intro to Probability and Stats
     - Fundamentals of ML for Tabular Data Development - Test Process, Validation, Evaluation Metrics
  - Week 2: Boosting for Tabular Data Decision Trees, Ensembles
  - Week 3: Building a Neural Network Neural Networks as data tranformation pipelines, regression and classification
  - Week 4: Training Deep Networks Tricks of the trade, Optimization Algorithms for training, Initialization
