.. image:: https://cdn.extras.talentsprint.com/CentralRepo/images/TS_updated_logo.png
  :width: 200

================================
Program Design Document Template
================================
This is part of the official standard documentation of TalentSprint.
Except when explicitly specified, the contents are copyrighted intellectual
property of TalentSprint Private Limited.

Metadata
--------

:Description: Program Design Document Template

:Author: Asokan Pichai

:Version: 1.0.1

:See also: Core_Docs --> RepoLayout

Purpose
-------
    This document is the template for all Program Design Documents

    
Template
--------

:Metadata: As above

:Program Name: AI and MLOps

:Audience: 
  - Professionals looking to advance their skills in machine learning operations
  - Professionals looking to build expertise in end-to-end machine learning systems for real-world applications
  - An AI and Data Science practitioner seeking to build expertise in AI and MLOps 
  - A Tech professional looking to transition to AI and MLOps 
  - A Tech ops professional aspiring to upgrade to AI and MLOps

:Selection Process: 
  - Eligibility
     - B.E/B.Tech/M.E/M.Tech or equivalent master's degree with a minimum 50% marks
     - Minimum 2 year of professional experience
     - Basic coding knowledge required
  - Once eligible
     - Apply for the Programme
     - Submit Documents
     - Selection by IISc Committee
     - Join the Programme
  - Selection for the programme will be done by IISc and is strictly based on the education, work experience, and motivation of the participants(statement of purpose submitted along with the application)
  - Scanned copies to be submitted within 7 days - 1. Education Certificate 2. Experience Letter/Latest Pay Slip

:Objectives: 
  - The programme enables you to accelerate your professional growth. It allows you to
     - Design AI/ML models from scoping to deployment
     - Identify gaps in creating and scaling AI/ML models
     - Evaluate and improve AI/ML models for projects
  - Upon becoming adept at MLOps implementation, you will be able to
     - Improve team collaboration
     - Streamline operations, enhance RoI
     - Increase reliability, performance, and scalability of AI/ML systems

:Coverage: 
  - The program comprises of following modules:
     - Module 0: Maths and Programming Pre-requisites
     - Module 1: Foundations of Machine Learning and Artificial Intelligence
     - Module 2: Computer Vision
     - Module 3: Designing Machine Learning Systems
     - Module 4: Practical MLOps
     - Module 5: Natural Language Processing
     - Module 6: Representation Learning, Generative Models and Research Trends
     - Module 7: Parallel Computer Architecture and Programming Models
     - Module 8: Machine Learning at Scale

:Assessment Strategies: Lecture Attendance, Mini-projects, Assignments, Quizzes, Long Quizzes, Capstone Project

:Certification: A certificate of successful completion is provided upon completion of all requirements of the programme. IISc carries out all examinations and evaluations related to the certification.

:References: 
  - Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow - Book by Aurélien Géron
  - Deep Learning with Python - Book by François Chollet
  - Practical MLOps, Operationalizing ML Models - Book by Noah Gift & Alfredo Deza
