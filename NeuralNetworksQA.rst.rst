**Neural Networks Theory Study Material in Q/A Form – Lectures**
           **Topics: MLP, CNN, RNN, PCA, t-SNE, Auto-encoders**

1.	Why is Logistic Regression classifier generally preferred than a Classical Perceptron (i.e., a single layer of threshold logic units) for classification? Suggest a tweak to the Perceptron so that it is useful for classification?

a.	A classical Perceptron will only produce a linear classifier, i.e., it can classify datasets that are linearly separable. In contrast, a Logistic Regression classifier will converge to a good solution even if the dataset is not linearly separable. Further, Logistic Regression classifier will output class probabilities. 

b.	If you change the Perceptron’s activation function to the logistic activation function (or the softmax activation function for multilabel classification), and if you train it using backpropagation algorithm with cross entropy loss function, then such a multilayer perceptron can classify non-linearly separable datasets. 




Stacking multiple linear layers without a non-linear activation function boils down to a single linear layer of perceptrons.


2.	Why is picking a non-linear activation function other than threshold logic unit important for training MLPs?

a.	The weights are learnt and updated using stochastic gradient descent, which requires gradients that are non zero to carry out the updates. Hence, initially, the logistic activation was used instead of  a threshold logic unit.
 

b.	The logistic activation function has a derivative that is always nonzero, whereas for the step function, the derivative is mostly zero.

3.	Name three popular activation functions.	
Popular activation functions include:- 

a.	The step function 
b.	The logistic (sigmoid) function 
c.	The hyperbolic tangent (tanh) function 
d.	The Rectified Linear Unit, (ReLU) function.
e.	ELU
f.	SELU


4.	Suppose you have an MLP composed of one input layer with 10 pass-through Neurons, followed by one hidden layer with 50 Neurons, and finally one output layer with 3 artificial neurons. All artificial neurons use the ReLU activation function.

a.	What is the shape of the input matrix X?

b.	What are the shapes of the hidden layer’s weight vector Wh and its bias vector
bh?

c.	What are the shapes of the output layer’s weight vector Wo and its bias vector
      bo?

d.	What is the shape of the network’s output matrix Y?

e.	Write the equation that computes the network’s output matrix Y as a function
of X, Wh, bh, Wo, and bo.

a.	The shape of the input matrix X is ‘m× 10’, where m represents the training batch size.

b.	The shape of the hidden layer’s weight vector Wh is 10 × 50, and the length of
                   
                   its bias vector bh is 50.
c.	The shape of the output layer’s weight vector Wo is 50 × 3, and the length of its
 bias vector bo is 3.

d.	The shape of the network’s output matrix Y is m × 3.

e.	Y = ReLU(ReLU(X Wh + bh) Wo + bo). 



Recall that the ReLU function just sets every negative number in the matrix to zero. Also note that when you are adding a bias vector to a matrix, it is added to every single row in the matrix, which is called broadcasting.
  
5.	What is backpropagation and how does it work? 

Backpropagation is a technique used to train artificial neural networks. It first computes the gradients of the cost function with regard to every model parameter (all the weights and biases), then it performs a Gradient Descent step using these gradients. This backpropagation step is typically performed thousands or millions of times, using many training batches, until the model parameters converge to values that (hopefully) minimize the cost function. 



To compute the gradients, backpropagation uses reverse-mode autodiff (although it wasn’t called that when backpropagation was invented, and it has been reinvented several times). Reverse-mode autodiff performs a forward pass through a computation
graph, computing every node’s value for the current training batch, and then it performs a reverse pass, computing all the gradients at once. 



So what’s the difference? 
Backpropagation refers to the whole process of training an artificial neural network in steps, each of which computes gradients and uses them to perform a Gradient Descent step. In contrast, reverse-mode autodiff is just a technique to compute gradients efficiently, and it happens to be used by backpropagation.



6.	What are the advantages of a CNN over a fully connected DNN for image classification?
These are the main advantages of a CNN over a fully connected DNN for image
classification:

a.	A CNN has much fewer parameters than a fully connected DNN because neurons are partially connected and because it heavily reuses its weights. This makes it much faster to train, reduces the risk of overfitting, and requires much less training data.

b.	When a CNN has learned a kernel that can detect a particular feature, it can detect that feature anywhere in the image. In contrast, when a DNN learns a feature in one location, it can detect it only in that particular location. Since images typically have very repetitive features, CNNs are able to generalize much better than DNNs for image processing tasks such as classification, using fewer training examples.

c.	Finally, a DNN has no prior knowledge of how pixels are organized; it does not know that nearby pixels are close. A CNN’s architecture embeds this prior knowledge. Lower layers typically identify features low-level features such as lines, curves; while higher layers combine the lower-level features into larger features. This works well with most natural images, giving CNNs a decisive headstart compared to DNNs.


7.	Consider a CNN composed of three convolutional layers, each with 3 × 3 kernels, a stride of 2, and "same" padding. The lowest layer outputs 100 feature maps, the middle one outputs 200, and the top one outputs 400. The input images are RGB images of 200 × 300 pixels.
Let’s compute how many parameters the CNN has:- 

a.	Since its first convolutional layer has 3 × 3 kernels, and the input has three channels (red, green, and blue), each feature map has 3 × 3 × 3 weights, plus a bias term. That’s 28 parameters per feature map. Since this first convolutional layer has 100 feature maps, it has a total of 2,800 parameters. 

b.	The second convolutional layer has 3 × 3 kernels and its input is the set of 100 feature maps of the previous layer, so each feature map has 3 × 3 × 100 = 900 weights, plus a bias term. Since it has 200 feature maps, this layer has 901 × 200 = 180,200 parameters. Finally, the third and last convolutional layer also has 3 × 3 kernels, and its input is the set of 200 feature maps of the previous layers, so each feature map has 3 × 3 × 200 = 1,800 weights, plus a bias term. Since it has 400 feature maps, this layer has a total of 1,801 × 400 = 720,400 parameters. 

All in all, the CNN has 2,800 + 180,200 + 720,400 = 903,400 parameters. 



8.	For the same example as above, what is the total number of parameters in the CNN? If we are using 32-bit floats, at least how much RAM will this network require when making a prediction for a single instance? What about when training on a mini-batch of 50 images?

a.	First let’s compute the feature map size for each layer. Since we are using a stride of 2 and "same" padding, the horizontal and vertical dimensions of the feature maps are divided by 2 at each layer (rounding up if necessary). 

b.	So, as the input channels are 200 × 300 pixels, the first layer’s feature maps are 100 × 150, the second layer’s feature maps are 50 × 75, and the third layer’s feature maps are 25 × 38. Since 32 bits is 4 bytes, the first convolutional layer has 100 feature maps, this first layer takes up 4 × 100 × 150 × 100 = 6 million bytes (6 MB). The second layer takes up 4 × 50 × 75 ×200 = 3 million bytes (3 MB). Finally, the third layer takes up 4 × 25 × 38 × 400 = 1,520,000 bytes (about 1.5 MB). 

c.	However, once a layer has been computed, the memory occupied by the previous layer can be released, so if everything is well optimized, only 6 + 3 = 9 million bytes (9 MB) of RAM will be required (when the second layer has just been computed, but the memory occupied by the first layer has not been released yet). Remember that you also need to add the memory occupied by the CNN’s parameters! We computed earlier that it has 903,400 parameters, each using up 4 bytes, so this adds 3,613,600 bytes (about 3.6 MB). The total RAM required is therefore (at least) 12,613,600 bytes (about 12.6 MB).


d.	Lastly, let’s compute the minimum amount of RAM required when training the CNN on a mini-batch of 50 images. During training TensorFlow uses backpropagation, which requires keeping all values computed during the forward pass until the reverse pass begins. So we must compute the total RAM required by all layers for a single instance and multiply that by 50. At this point, let’s start counting in megabytes rather than bytes. 

e.  We computed before, that the three layers require respectively 6, 3, and 1.5 MB for each instance. That’s a total of 10.5 MB per instance, so for 50 instances the total RAM required is 525 MB. Add to that the RAM required by the input images, which is 50 × 4 × 200 × 300 × 3 = 36 million bytes (36 MB), plus the RAM required for the model parameters, which is about 3.6 MB (computed earlier), plus some RAM for the gradients (we will neglect this since it can be released gradually as backpropagation goes down the layers during the reverse pass). We are up to a total of roughly 525 + 36 + 3.6 = 564.6 MB, and that’s really an optimistic bare minimum.



9.	If your GPU runs out of memory while training a CNN, what are five things you
could try to solve the problem?
If your GPU runs out of memory while training a CNN, here are five things you
could try to solve the problem (other than purchasing a GPU with more RAM):

a.	Reduce the mini-batch size.

b.	Reduce dimensionality using a larger stride in one or more layers.

c.	Remove one or more layers.

d.	Use 16-bit floats instead of 32-bit floats.

e.	Distribute the CNN across multiple devices.

10.	Why would you want to add a max pooling layer rather than a convolutional
layer with the same stride?

A max pooling layer has no parameters at all, whereas a convolutional layer has
quite a few.

11.	Can you think of a few applications for a sequence-to-sequence RNN? What about a sequence-to-vector RNN, and a vector-to-sequence RNN?
Here are a few RNN applications:

a.	For a sequence-to-sequence RNN: predicting the weather (or any other time series), machine translation (using an Encoder–Decoder architecture), video captioning, speech to text, music generation (or other sequence generation), identifying the chords of a song

b.	For a sequence-to-vector RNN: classifying music samples by music genre analysing the sentiment of a book review, predicting what word an aphasic patient is thinking of based on readings from brain implants, predicting the probability that a user will want to watch a movie based on their watch history (this is one of many possible implementations of collaborative filtering for  a recommender system)

c.	For a vector-to-sequence RNN: image captioning, creating a music playlist based on an embedding of the current artist, generating a melody based on a set of parameters, locating pedestrians in a picture (e.g., a video frame from a self-driving car’s camera)



12.	How many dimensions must the inputs of an RNN layer have? What does each dimension represent? What about its outputs?

a.	An RNN layer must have three-dimensional inputs: 

1.	The first dimension is the batch dimension (its size is the batch size). 

2.	The second dimension represents the time (its size is the number of time steps).

3.	The third dimension holds the inputs at each time step (its size is the number of input features per time step).

b.	For example, if you want to process a batch containing 5 time series of 10 time steps each, with 2 values per time step (e.g., the temperature and the wind speed), the shape will be [5, 10, 2]. 

c.	The outputs are also three-dimensional, with the same first two dimensions, but the last dimension is equal to the number of neurons. For example, if an RNN layer with 32 neurons processes the batch we just discussed, the output will have a shape of [5, 10, 32].



13.	If you want to build a deep sequence-to-sequence RNN, which RNN layers should have return_sequences=True? What about a sequence-to-vector RNN?

a.	To build a deep sequence-to-sequence RNN using Keras, you must set return_sequences=True for all RNN layers. 
b.	To build a sequence-to-vector RNN, you must set return_sequences=True for all RNN layers except for the top RNN layer, which must have return_sequences=False (or do not set this argument at all, since False is the default).



14.	Suppose you have a daily univariate time series, and you want to forecast the next seven days. Which RNN architecture should you use?

If you have a daily univariate time series, and you want to forecast the next seven days, the simplest RNN architecture you can use is a stack of RNN layers (all with return_sequences=True except for the top RNN layer), using seven neurons in the output RNN layer. You can then train this model using random windows from the time series (e.g., sequences of 30 consecutive days as the inputs, and a vector containing the values of the next 7 days as the target). This is a sequence to-vector RNN. 

Alternatively, you could set return_sequences=True for all RNN layers to create a sequence-to-sequence RNN. You can train this model using random windows from the time series, with sequences of the same length as the inputs as the targets. Each target sequence should have seven values per time step (e.g., for time step t, the target should be a vector containing the values at time steps t + 1 to t + 7).



15.	What are the main difficulties when training RNNs? How can you handle them?

The two main difficulties when training RNNs are:- 

a.	Unstable gradients (exploding or vanishing) 

b.	A very limited short-term memory. 

These problems both get worse when dealing with long sequences. 

To alleviate the unstable gradients problem:- 

a.	You can use a smaller learning rate 

b.	Use a saturating activation function such as the hyperbolic tangent (which is the default). 

c.	Possibly use gradient clipping, Layer Normalization, or dropout at each time step. 


To tackle the limited short-term memory problem:- 

a.	You can use LSTM or GRU layers (this also helps with the unstable gradients problem).



16.	Can you sketch the LSTM cell’s architecture?

An LSTM cell’s architecture looks complicated, but it’s actually not too hard if you understand the underlying logic. The cell has a short-term state vector and a long-term state vector. 
At each time step, the inputs and the previous short-term state are fed to a simple RNN cell and three gates: 

a.	The forget gate decides what to remove from the long-term state. 

b.	The input gate decides which part of the output of the simple RNN cell should be added to the long-term state. 

c.	The output gate decides which part of the long-term state should be output at this time step (after going through the tanh activation function). The new short-term state is equal to the output of the cell.

.. image:: Images/image3.png
   :width: 100px
   :align: center 

17.	What are the main motivations for reducing a dataset’s dimensionality? What are
the main drawbacks?

The main motivations for dimensionality reduction are:

a.	To speed up a subsequent training algorithm (in some cases it may even remove noise and redundant features, making the training algorithm perform better)

b.	To visualize the data and gain insights on the most important features

c.	To save space (compression)

The main drawbacks are:

a.	 Some information is lost, possibly degrading the performance of subsequent
training algorithms.

b.	It can be computationally intensive.

c.	It adds some complexity to your Machine Learning pipelines.

d.	Transformed features are often hard to interpret.


18.	Once a dataset’s dimensionality has been reduced, is it possible to reverse the operation? If so, how? If not, why?

a.	Once a dataset’s dimensionality has been reduced using one of the algorithms we discussed, it is almost always impossible to perfectly reverse the operation, because some information gets lost during dimensionality reduction. 

b.	Moreover, while some algorithms (such as PCA) have a simple reverse transformation procedure that can reconstruct a dataset relatively similar to the original, other algorithms (such as T-SNE) do not.



19.	Can PCA be used to reduce the dimensionality of a highly nonlinear dataset?

a.	PCA can be used to significantly reduce the dimensionality of most datasets, even if they are highly nonlinear, because it can at least get rid of useless dimensions. However, if there are no useless dimensions—as in a Swiss roll dataset—then reducing dimensionality with PCA will lose too much information. You want to unroll the Swiss roll, not squash it.

20.	Suppose you perform PCA on a 1,000-dimensional dataset, setting the explained variance ratio to 95%. How many dimensions will the resulting dataset have?

That’s a trick question: 
It depends on the dataset. Let’s look at two extreme examples:-

a.	First, suppose the dataset is composed of points that are almost perfectly aligned. In this case, PCA can reduce the dataset down to just one dimension while still preserving 95% of the variance. 

b.	Now imagine that the dataset is composed of perfectly random points, scattered all around the 1,000 dimensions. In this case roughly 950 dimensions are required to preserve 95% of the variance. 

So the answer is, it depends on the dataset, and it could be any number between 1 and 950. Plotting the explained variance as a function of the number of dimensions is one way to get a rough idea of the dataset’s intrinsic dimensionality.


21.	What is t-SNE and give its full form.

a.	t-SNE reduces dimensionality while trying to keep similar instances close and dissimilar instances apart. It is mostly used for visualization, in particular to visualize clusters of instances in high-dimensional space (e.g., to visualize the MNIST images in 2D).

b.	t-Distributed Stochastic Neighbour Embedding


we can include a question on how t-sne is performed: Kullback-Leibler cost, perplexity, gradient descent…


22.	What are the main tasks that autoencoders are used for?

Here are some of the main tasks that autoencoders are used for:

a.	Feature extraction

b.	Unsupervised pretraining

c.	Dimensionality reduction

d.	Generative models

e.	Anomaly detection 



23.	Suppose you want to train a classifier, and you have plenty of unlabelled training
data but only a few thousand labelled instances. How can autoencoders help?
How would you proceed?

If you want to train a classifier and you have plenty of unlabelled training data but only a few thousand labelled instances, then:- 


a. you could first train a deep autoencoder on the full dataset (labelled + unlabelled), 

b.	Then reuse its lower half for the classifier (i.e., reuse the layers up to the codings layer, included) and train the classifier using the labelled data. If you have little labelled data, you probably want to freeze the reused layers when training the classifier.



24.	If an autoencoder perfectly reconstructs the inputs, is it necessarily a good autoencoder? How can you evaluate the performance of an autoencoder?

a.	The fact that an autoencoder perfectly reconstructs its inputs does not necessarily mean that it is a good autoencoder; 

b.	Perhaps it is simply an over-complete autoencoder that learned to copy its inputs to the codings layer and then to the outputs.

c.	In fact, even if the codings layer contained a single neuron, it would be possible for a very deep autoencoder to learn to map each training instance to a different coding (e.g., the first instance could be mapped to 0.001, the second to 0.002, the third to 0.003, and so on), and it could learn “by heart” to reconstruct the right training instance for each coding. 
It would perfectly reconstruct its inputs without really learning any useful pattern in the data. In practice such a mapping is unlikely to happen, but it illustrates the fact that perfect reconstructions are not a guarantee that the autoencoder learned anything useful. However, if it produces very bad reconstructions, then it is almost guaranteed to be a bad autoencoder.

To evaluate the performance of an autoencoder, one option is to measure the reconstruction loss (e.g., compute the MSE, or the mean square of the outputs minus the inputs). Again, a high reconstruction loss indicates that the autoencoder is bad, but a low reconstruction loss is not a guarantee that it is good. You should also evaluate the autoencoder according to what it will be used for. For example, if you are using it for unsupervised pretraining of a classifier, then you should also evaluate the classifier’s performance.


25.	What are under-complete and over-complete autoencoders? What is the main risk of an excessively under-complete autoencoder? What about the main risk of an over-complete autoencoder?

a.	An under-complete autoencoder is one whose codings layer is smaller than the input and output layers. If it is larger, then it is an over-complete autoencoder.

b.	The main risk of an excessively under-complete autoencoder is that it may fail to
reconstruct the inputs. 

c.	The main risk of an over-complete autoencoder is that it may just copy the inputs to the outputs, without learning any useful features.



26.	How do you tie weights in a stacked autoencoder? What is the point of doing so?
To tie the weights of an encoder layer and its corresponding decoder layer, you simply make the decoder weights equal to the transpose of the encoder weights. This reduces the number of parameters in the model by half, often making training converge faster with less training data and reducing the risk of overfitting the training set.



27.	What is a generative model? Can you name a type of generative autoencoder?

a.	A generative model is a model capable of randomly generating outputs that resemble the training instances. 

b.	For example, once trained successfully on the MNIST dataset, a generative model can be used to randomly generate realistic images of digits. The output distribution is typically similar to the training data.

c.	For example, since MNIST contains many images of each digit, the generative model would output roughly the same number of images of each digit. Some generative models can be parametrized—for example, to generate only some kinds of outputs. 

d.	An example of a generative autoencoder is the variational autoencoder.



28.	How do we pick an appropriate neural network model for our task?

1.	MLPs

a.	An MLP is a fully connected (FC) network. MLPs are common in simple logistic and linear regression problems. They are very flexible and can be used  generally to learn a mapping from inputs to outputs.

b.	MLPs are used for Tabular datasets, Classification prediction problems and Regression prediction problems. MLPs are suitable for classification prediction problems where inputs are assigned a class or label.

c.	This flexibility allows them to be applied to other types of data. For example, the pixels of an image can be reduced to one long row of data and fed into a MLP. The words of a document can also be reduced to one long row of data and fed to a MLP. Even the lag observations for a time series prediction problem can be reduced to a long row of data and fed to a MLP.

d.	MLPs are also suitable for regression prediction problems where a real-valued quantity is predicted given a set of inputs.

e.	As such, if your data is in a form other than a tabular dataset, such as an image, document, or time series, it is recommended to test an MLP on your problem. The results can be used as a baseline point of comparison to confirm that other models that may appear better suited add value.

f.	However, MLPs are not optimal for processing sequential and multi-dimensional data patterns. By design, an MLP struggles to remember patterns in sequential data and requires a substantial number of parameters to process multi-dimensional data. 

2.	RNNs

g.	For sequential data input, RNNs are popular because the internal design allows the network to discover dependency in the history of the data, which is useful for prediction. 

h.	Recurrent Neural Networks, or RNNs, were designed to work with sequence prediction problems. Sequence prediction problems come in many forms and are best described by the types of inputs and outputs supported. Some examples of sequence prediction problems include:

•	One-to-Many: An observation as input mapped to a sequence with multiple steps as an output.

•	Many-to-One: A sequence of multiple steps as input mapped to class or quantity prediction.

•	Many-to-Many: A sequence of multiple steps as input mapped to a sequence with multiple steps as output.

3.	CNNs

i.	For multi-dimensional data like images and videos, CNNs excel in extracting feature maps for classification, segmentation, generation, and other downstream tasks. In some cases, a CNN in the form of a 1D convolution is also used for networks with sequential input data. 

j.	The benefit of using CNNs is their ability to develop an internal representation of a two-dimensional image. This allows the model to learn position and scale invariant structures in the data, which is important when working with images.

k.	More generally, CNNs work well with data that has a spatial relationship. The CNN input is traditionally two-dimensional, a field or matrix, but can also be changed to be one-dimensional, allowing it to develop an internal representation of a one-dimensional sequence. This allows the CNN to be used more generally on other types of data that has a spatial relationship. For example, there is an order relationship between words in a document of text. There is an ordered relationship in the time steps of a time series. Although not specifically developed for non-image data, CNNs achieve state-of-the-art results on problems such as document classification used in sentiment analysis and related problems.



29.	When do we apply the ReLU activation function vs the Softmax function?
Consider the MNIST dataset: MNIST is a collection of handwritten digits ranging from 0 to 9. Images of the MNIST digits are each sized at 28 x 28 - pixels, in grayscale. It has a training set of 60,000 images, and 10,000 test images that are classified into corresponding categories or labels.

1.	ReLU

a.	The MNIST digit classification is inherently a non-linear process. Since a Dense layer is a linear operation, a sequence of Dense layers can only approximate a linear function. So, inserting a ReLU activation between the Dense layers will enable an MLP network to model non-linear mappings. 

b.	ReLU is a simple non-linear function. It's very much like a filter that allows positive inputs to pass through unchanged while clamping everything else to zero. Mathematically, ReLU is expressed in the following equation and is plotted as shown in the figure below.

c.	There are other non-linear functions that can be used, such as elu, selu, softplus, sigmoid, and tanh. However, ReLU is the most commonly used function and is computationally efficient due to its simplicity.

.. image:: Images/image4.png
   :width: 100px
   :align: center 
                      

2.	Softmax 

a.	In the MNIST digit classification task, the output layer has 10 units followed                      by a softmax activation layer. The 10 units correspond to the 10 possible labels, classes, or categories. The softmax activation can be expressed mathematically, as shown in the following equation:  

.. image:: Images/image5.png
   :width: 100px
   :align: center 

b.	Here, the z represents the values from the neurons of the output layer. The exponential acts as the non-linear function. Later these values are divided by the sum of exponential values in order to normalize and then convert them into probabilities. Note that, when the number of classes is two, it becomes the same as the sigmoid activation function. In other words, sigmoid is simply a variant of the Softmax function.

c.	The equation is applied on all N = 10 outputs, xi for i = 0, 1 … 9 for the final prediction. The idea of softmax is surprisingly simple. It squashes the outputs into probabilities by normalizing the prediction. Here, each predicted output is a probability that the index is the correct label of the given input image. The sum of all the probabilities for all outputs is 1.0.














